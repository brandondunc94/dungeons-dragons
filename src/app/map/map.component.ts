import { Component, OnInit, Output } from '@angular/core';
import { CharacterComponent } from '../character/character.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  @Output() characters = Array<CharacterComponent>();

  constructor() { 
    this.characters = [];
  }

  ngOnInit(): void {
  }

  createNewCharacter() {
    let newCharacter = new CharacterComponent();
    newCharacter.name = 'Brandor';
    this.characters.push(newCharacter);

  }

}
